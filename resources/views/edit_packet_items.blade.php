<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Edit Packet Items</title>
    </head>
    <body>
        <table>
            <tr>
                <td>Packet Name</td>
                <td>Packet Items</td>
                <td>Action</td>
            </tr>
            <tr>
                <td>{{$packet->packet_name}}</td>
                <td>
                    <ul>
                        @foreach($items as $item)
                        <li>
                            {{$item->food->food_name}}
                            <a
                                href="/remove-packet-item/{{$item->id}}/{{$packet->id}}"
                                ><button>Remove</button></a
                            >
                        </li>

                        @endforeach
                    </ul>
                </td>
                <td>
                    <form
                        action="/update-packet-items/{{$packet->id}}"
                        method="POST"
                    >
                        @csrf
                        <select name="food_id">
                            @foreach($foods as $food)
                            <option
                                value="{{$food->id}}"
                                >{{$food->food_name}}</option
                            >
                            @endforeach
                        </select>
                        <button type="submit">Add New Food</button>
                    </form>
                </td>
            </tr>
        </table>

        <a href="/home"><button>Return to home</button></a>
    </body>
</html>
