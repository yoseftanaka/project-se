<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Food</title>
</head>
<body>
    <form action="/store-food" method="POST" enctype="multipart/form-data">
        @csrf
        <input name="food_name" placeholder="Food Name"><br>
        <input name="image" type="file"><br>
        <textarea name="description"></textarea><br>
        <input name="price" type="text" placeholder="price"><br>
        <button type="submit">Submit</button>
    </form>
</body>
</html>