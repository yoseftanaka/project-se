<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>View Packet</title>
    </head>
    <body>
        <div>
            <p>{{$packet->custom_packet_name}} RP. {{$packet->price}} for 30 days</p> 
            @foreach($items as $item)
            <p>{{$item->food->food_name}}</p>
            @endforeach
        </div>
        <div>
            <a href="/user/home"><button>Back</button></a>
        </div>
    </body>
</html>
