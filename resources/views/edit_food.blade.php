<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Document</title>
    </head>
    <body>
        <form
            action="/update-food/{{$food->id}}"
            method="POST"
            enctype="multipart/form-data"
        >
            @csrf
            <input name="food_name" placeholder="{{$food->food_name}}" /><br />
            <input name="image" type="file" /><br />
            <textarea
                name="description"
                placeholder="{{$food->description}}"
            ></textarea
            ><br />
            <button type="submit">Update</button>
        </form>
    </body>
</html>
