<!DOCTYPE html>
<html lang="en">
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/TaskOrder.css">
    <script src="main.js"></script>


</head>


<body>


    <div class="mainPage">

        <div class="Nav">
            <div class="Navbar-container">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="opacity: 0.8;">

                    <a class="navbar-brand" href="#">Cateringku.com</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home</a>
                            </li>
                            <li class="nav-item active">

                                <div class="dropdown">
                                    <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown">
                                        Package
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">Reguler</a>
                                        <a class="dropdown-item" href="#">Paket</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="#about">List Menu</a>
                            </li>

                        </ul>


                     
                        <div class="Nav-Right">
                            <ul class="navbar-nav ">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Profile</a>
                                </li>
                                <form method="POST" action="/logout">
                                    @csrf
                                <li class="nav-item active">
                                    <a class="nav-link" href="#" style="background-color: white"> <button class="logout"><img
                                            src="/storage/publicly/logout.png" width: 200px height=30 px alt=""></button></a>
                                </li>
                                </form>
                            </ul>
                        </div>

                    </div>
                </nav>



            </div>

        </div>



        <div class=" mainPageContainer">
            <div class="Order">
                <div class="myOrder"> <a class="myOrders">My Order</a> </div>
                <div class="taskOrder"> <a class="taskOrders">Task Order</a> </div>
                <div class="myPayment"> <a class="myPayments"> My Payment</a></div>

            </div>
        </div>


        <div class="contentTengah">

            <div class="container">
                <form action="/user/store-transaction/{{$saved_id}}" method="POST">
                    @csrf
                    <table>
                        <tr>
                            <td>
                                <label>Nama</label>
                            </td>
                            <td>
                                <input type="text" class="form-control" id="first_name" name="nama">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Alamat</label>
                            </td>
                            <td>
                                <textarea type="text" class="form-control" id="first_name" name="alamat"
                                    style="resize: none"> </textarea>

                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label>No Telepon</label>
                             </td>
                            <td>
                                <input type="text" class="form-control" id="first_name" name="telepon">
                            </td>
                        </tr>

                        <tr>
                            <td>
                                <label>Pilih Jumlah Paket</label>
                            </td>
                            <td>
                                
                                <select id="Quantity" required  style="margin-left: 15px " name="quantity">
                                    <option value="" disabled selected>Jumlah Paket</option>
                                    <option value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                </select>
                            </td>
                        </tr>

                    </table>
                    <div class="btnSubmit">
                        <button type="submit" class="btn btn-primary"
                            style="display: block; margin: auto">Submit</button>
                    </div>


            </div>






            </form>
        </div>




    </div>






    </div>
    
    <footer id="footer" class="footer">


        <div class="copyright">

            <p>Made with <i class="fa fa-heart" style="width: 20px; height:20px;color: red;"></i> by
                Cateringku.com </a>2019. All Rights
                Reserved</p>
        </div>

    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


</body>

</html>