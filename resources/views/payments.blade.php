<!DOCTYPE html>
<html lang="en">
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/Payment.css">
    <script src="main.js"></script>


</head>


<body>


    <div class="mainPage">

        <div class="Nav">
            <div class="Navbar-container">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="opacity: 0.8;">

                    <a class="navbar-brand" href="#">Cateringku.com</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home</a>
                            </li>
                            <li class="nav-item active">

                                <div class="dropdown">
                                    <button type="button" class="btn btn-dark dropdown-toggle" data-toggle="dropdown">
                                        Package
                                    </button>
                                    <div class="dropdown-menu">
                                        <a class="dropdown-item" href="#">Reguler</a>
                                        <a class="dropdown-item" href="#">Paket</a>
                                    </div>
                                </div>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="#about">List Menu</a>
                            </li>

                        </ul>


                        <div class="Nav-Right">
                            <ul class="navbar-nav ">
                                <li class="nav-item active">
                                    <a class="nav-link" href="#">Profile</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="#" style="background-color: white"> <img src="assets/img/logout.png" width: 200px height= 30 px alt=""></a>
                                </li>
                            </ul>
                        </div>

                    </div>
                </nav>



            </div>

        </div>
        <div class=" mainPageContainer">
            <div class="Order">
                <div class="myOrder"> <a class="myOrders">My Order</a> </div>
                <div class="taskOrder"> <a class="taskOrders">Task Order</a> </div>
                <div class="myPayment"> <a class="myPayments"> My Payment</a></div>

            </div>
        </div>
        <div class="contentTengah">

            <div class="container">
            <form action="/user/complete-transaction/{{$headtransaction_id}}" method="POST">
                @csrf
                    <table>
                        <tr>
                            <td>
                               <h3>List Ordered   :</h2>
                            </td>
                            
                        </tr>
                        @foreach ($packet_price as $item)
                        <tr>
                                <td>
                                <label>{{$item->packet->price}} X {{$item->quantity}}</label>
                                </td>
                            </tr>
                        @endforeach
                        
                        
                        <tr>
                            <td>
                            ______________________
                             </td>
                        </tr>
                        <tr>
                            <td>
                                <label id="hargaTotal">RP . {{$total}}</label>
                             </td>
                        </tr>
                        <tr>
                            <td>
                                <label>Pilih Transaksi Pembayaran</label>
                            </td>
                           
                        </tr>
                        <tr>
                            <td>
                                <select name="payment_type" id="Quantity" required>
                                    <option value="" disabled selected>Pilih Rekening</option>
                                    <option value="Bank BCA 12121212">Bank BCA 12121212</option>
                                    <option value="BANK BRI 21212122">BANK BRI 21212122</option>
                                </select>
                            </td>
                        </tr>
                    </table>



                    <div class="btnSubmit">
                        <button type="submit" class="btn btn-primary"
                            style="display: block; margin: auto">Submit</button>
                    </div>


            </div>

            </form>
        </div>




    </div>




    </div>
    
    <footer id="footer" class="footer">


        <div class="copyright">

            <p>Made with <i class="fa fa-heart" style="width: 20px; height:20px;color: red;"></i> by
                Cateringku.com </a>2019. All Rights
                Reserved</p>
        </div>

    </footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


</body>

</html>