
<!DOCTYPE html>
<html lang="en">
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="css/software.css">
    <script src="main.js"></script>


</head>


<body>
    <div class="landingPage">
        <div class="Nav">
            <div class="Navbar-container">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="opacity: 0.8;">

                    <a class="navbar-brand" href="#"> <img src="/storage/publicly/Logo.jpg"  style="width: 50px; height: 50px; border-radius: 10%;" alt=""></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home</a>
                            </li>

                            <li class="nav-item active">
                                <a class="nav-link" href="#about">About</a>
                            </li>

                        </ul>


                        <div class="Nav-Right">
                            <ul class="navbar-nav ">
                                <li class="nav-item active">
                                    <a class="nav-link" href="/login">Login</a>
                                </li>
                                <li class="nav-item active">
                                    <a class="nav-link" href="/register">Register</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </nav>



            </div>

        </div>

        <div class=" LandingPageContainer">
                                        <div class="LandingPageTittle">
                                            <h1>Cateringku.com</h1>
                                        </div>
                                        <div class="LandingPageText">
                                            Catheringku merupakan website yang bergerak di bidang pelayanan kuliner ynag berbasis website. Dalam pelayanan kami, kami menyediakan menu-menu yang disajikan, yang terdiri dari menu paket dan menu single. Di samping itu, menu yang disajikan oleh website kami bisa dicustom oleh customer.
                                        </div>
                        </div>




                    </div>





                    <section id="about" class="about">
                        <div class="aboutContainer">


                            <div class="aboutContainerDalam">
                                <div class="aboutPhoto">
                                    <img src="/storage/publicly/Logo.jpg"  alt="">
                                </div>
                                <div class="aboutText">
                                    <h1>About us</h1>
                                    Lorem ipsum dolor sit amet consectetur adipisicing elit. Commodi sequi omnis totam,
                                    qui repellendus dolor deleniti quidem odit sit quisquam provident inventore
                                    doloribus modi esse unde excepturi optio? Magni, perspiciatis!
                                </div>
                            </div>
                        </div>
                    </section>

                    <section id="contactUs" class="contactUs">
                        <div class="contactUsContainer">
                            <div class="contactUsText">
                                Contact Us
                            </div>

                            <div class="contactUsContent">
                                <table>
                                    <tr>
                                        <td class="logo"> <i class="fa fa-address-card-o " aria-hidden="true"></i></td>
                                        <td>Jalan Bina Nusantara </td>
                                    </tr>
                                    <tr>
                                        <td class="logo"> <i class="fa fa-phone" aria-hidden="true"></i></td>
                                        <td>021-4502121 </td>
                                    </tr>

                                    <tr>
                                        <td class="logo"><i class="fa fa-whatsapp" aria-hidden="true"></i></td>
                                        <td>08123676542 </td>
                                    </tr>

                                    <tr>
                                        <td class="logo"><i class="fa fa-facebook-square" aria-hidden="true"></i></td>
                                        <td>Cateringku.com </td>
                                    </tr>

                                    <tr>
                                        <td class="logo"><i class="fa fa-instagram" aria-hidden="true"></i></td>
                                        <td>Cateringku.com </td>
                                    </tr>







                                </table>




                            </div>

                        </div>
                    </section>


                    <footer id="footer" class="footer">


                        <div class="copyright">

                            <p>Made with <i class="fa fa-heart" style="width: 20px; height:20px;color: red;"></i> by
                                Cateringku.com </a>2019. All Rights
                                Reserved</p>
                        </div>

                    </footer>

                    <!-- Optional JavaScript -->
                    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
                    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>

</body>

</html>