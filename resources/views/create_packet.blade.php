<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Create Packet</title>
</head>
<body>
    <form method="POST" action="/store-packet">
        @csrf
        <input name="packet_name" placeholder="Packet Name"><br>
        <input name="price" placeholder="Packet Price"><br>
        <select name ="duration">
            <option value="pagi">Pagi</option>
            <option value="siang">Siang</option>
            <option value="malam">Malam</option>
        </select>
        <button type="submit">Submit</button>
    </form>
</body>
</html>