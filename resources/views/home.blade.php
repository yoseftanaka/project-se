@extends('layouts.app') @section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                    <div class="alert alert-success" role="alert">
                        {{ session("status") }}
                    </div>
                    @endif

                    <div>
                        <p>Foods Registered</p>
                        <table>
                            <tr>
                                <td>Name</td>
                                <td>Image</td>
                                <td>Description</td>
                                <td>Price</td>
                                <td>Action</td>
                            </tr>
                            @foreach($foods as $food)
                            <tr>
                                <td>{{$food->food_name}}</td>
                                <td>
                                    <img
                                        src="/storage/{{$food->image}}"
                                        width="200px"
                                    />
                                </td>
                                <td>{{$food->description}}</td>
                                <td>{{$food->price}}</td>
                                <td>
                                    <a href="/edit-food/{{$food->id}}"
                                        ><button>Edit</button></a
                                    ><a href="/delete-food/{{$food->id}}"
                                        ><button>Delete</button></a
                                    >
                                </td>
                            </tr>
                            @endforeach
                        </table>

                        <a href="/create-food"
                            ><button>Create New Food</button></a
                        >
                    </div>
                    <div>
                        <p>Packet Registered</p>
                        <table>
                            <tr>
                                <td>Packet Name</td>
                                <td>Packet Items</td>
                                <td>Price</td>
                                <td>Action</td>
                            </tr>
                            @foreach($packets as $packet)
                            <tr>
                                <td>{{$packet->packet_name}}</td>
                                <td>
                                    <ul>
                                        @foreach($packet->detailPacket as $item)
                                    <li>{{$item->food->food_name}}</li>
                                        @endforeach
                                    </ul>
                                </td>
                                <td>
                                    RP. {{$packet->price}}/day
                                </td>
                                <td>
                                    <a href="edit-packet-name/{{$packet->id}}"
                                        ><button>Edit Packet Name & Price</button></a
                                    ><a href="edit-packet-items/{{$packet->id}}"
                                        ><button>Edit Packet Items</button></a
                                    >
                                    <a href="delete-packet/{{$packet->id}}"
                                        ><button>Delete Packet</button></a
                                    >
                                </td>
                            </tr>
                            @endforeach
                        </table>

                        <a href="create-packet"
                            ><button>Create new Packet</button></a
                        >
                    </div>
                    <div>
                        <form action="/logout" method="POST">
                            @csrf
                            <button type="submit">Logout</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
