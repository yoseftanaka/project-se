{{-- <!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Document</title>
    </head>
    <body>
        <div class="profile">
            {{$user->name}}
            <br />{{$user->email}}
        </div>

        <div>Paket pagi</div>
        @foreach($pagi as $a)
        <div>
            <a href="/user/view-packet/{{$a->id}}"
                ><span
                    >{{$a->packet_name}}<br />
                    Rp. {{$a->price}}/day</span
                ></a
            >
        </div>
        @endforeach
        @foreach($customPagi as $a)
        <div>
            <a href="/user/custom-view-packet/{{$a->id}}"
                ><span
                    >{{$a->custom_packet_name}}<br />
                    Rp. {{$a->price}}</span
                ></a
            >
        </div>
        @endforeach
        <div>Paket siang</div>
        @foreach($siang as $a)
        <div>
            <a href="/user/view-packet/{{$a->id}}"
                ><span
                    >{{$a->packet_name}}<br />
                    Rp. {{$a->price}}/day</span
                ></a
            >
        </div>
        @endforeach
        @foreach($customSiang as $a)
        <div>
            <a href="/user/custom-view-packet/{{$a->id}}"
                ><span
                    >{{$a->custom_packet_name}}<br />
                    Rp. {{$a->price}}</span
                ></a
            >
        </div>
        @endforeach
        <div>Paket malam</div>
        @foreach($malam as $a)
        <div>
            <a href="/user/view-packet/{{$a->id}}"
                ><span
                    >{{$a->packet_name}}<br />
                    Rp. {{$a->price}}/day</span
                ></a
            >
        </div>
        @endforeach
        @foreach($customMalam as $a)
        <div>
            <a href="/user/custom-view-packet/{{$a->id}}"
                ><span
                    >{{$a->custom_packet_name}}<br />
                    Rp. {{$a->price}}</span
                ></a
            >
        </div>
        @endforeach
        <div>
            Custom
        </div>
        <form action="/user/store-custom-packet/{{$user->id}}" method="POST">
            @csrf
            <div>
                <p>Paket pagi</p>
                <div>
                    <input
                        type="text"
                        name="custom_packet_name"
                        placeholder="packet name"
                    />
                    <br />
                    <input type="hidden" name="duration" value="pagi" />
                    @foreach($foods as $food)
                    <input
                        type="checkbox"
                        name="custom_food[]"
                        value="{{$food->id}}"
                    />{{$food->food_name}}<br />
                    @endforeach
                </div>
                <button type="submit">Submit</button>
            </div>
        </form>
        <form action="/user/store-custom-packet/{{$user->id}}" method="POST">
            @csrf
            <div>
                <p>Paket siang</p>
                <div>
                    <input
                        type="text"
                        name="custom_packet_name"
                        placeholder="packet name"
                    />
                    <br />
                    <input type="hidden" name="duration" value="siang" />
                    @foreach($foods as $food)
                    <input
                        type="checkbox"
                        name="custom_food[]"
                        value="{{$food->id}}"
                    />{{$food->food_name}}<br />
                    @endforeach
                </div>
                <button type="submit">Submit</button>
            </div>
        </form>
        <form action="/user/store-custom-packet/{{$user->id}}" method="POST">
            @csrf
            <div>
                <p>Paket malam</p>
                <div>
                    <input
                        type="text"
                        name="custom_packet_name"
                        placeholder="packet name"
                    />
                    <br />
                    <input type="hidden" name="duration" value="malam" />
                    @foreach($foods as $food)
                    <input
                        type="checkbox"
                        name="custom_food[]"
                        value="{{$food->id}}"
                    />{{$food->food_name}}<br />
                    @endforeach
                </div>
                <button type="submit">Submit</button>
            </div>
        </form>
        <form method="POST" action="/logout">
            @csrf
            <button type="submit">Logout</button>
        </form>
    </body>
</html> --}}

<!DOCTYPE html>
<html lang="en">
<!-- Required meta tags -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Bootstrap CSS -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">

<link href='https://fonts.googleapis.com/css?family=Pacifico' rel='stylesheet' type='text/css'>

<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">


<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Project</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" media="screen" href="/css/LoginPage.css">
    <script src="main.js"></script>


</head>


<body>


    <div class="mainPage">

        <div class="Nav">
            <div class="Navbar-container">
                <nav class="navbar navbar-expand-lg navbar-dark bg-dark" style="opacity: 0.8;">

                    <a class="navbar-brand" href="#">Cateringku.com</a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false"
                        aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>

                    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
                        <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
                            <li class="nav-item active">
                                <a class="nav-link" href="#">Home</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="#ourMenu">Package</a>
                            </li>
                            <li class="nav-item active">
                                <a class="nav-link" href="#about">List Menu</a>
                            </li>

                        </ul>


                        <div class="Nav-Right">
                            <ul class="navbar-nav ">
                                <li class="nav-item active">
                                <a class="nav-link" href="#">{{Auth::user()->name}}</a>
                                </li>
                                <form method="POST" action="/logout">
                                    @csrf
                                <li class="nav-item active">
                                    <a class="nav-link" href="#" style="background-color: white"> <button class="logout"><img
                                            src="/storage/publicly/logout.png" width: 200px height=30 px alt=""></button></a>
                                </li>
                                </form>
                            </ul>
                        </div>

                    </div>
                </nav>



            </div>

        </div>



        <div class=" mainPageContainer">
            <div class="Order">
                <div class="myOrder"> <a class="myOrders">My Order</a> </div>
                <div class="taskOrder"> <a class="taskOrders">Task Order</a> </div>
                <div class="myPayment"> <a class="myPayments"> My Payment</a></div>

            </div>
        </div>


        <div class="containerMenu">

            <div class="container1">
                <div class="headerContainer1">Menu Default</div>
                <div class="contentContainer1">

                <form action="/user/forward-packet/{{$user->id}}" method="post" id="form1">
                    @csrf
                        <label class="package-checkbox">
                            <h4>Paket Pagi</h4>

                            @foreach ($pagi as $item)
                        <label class="checkBoxCustom">
                            <input type="checkbox" name="default_packet[]" value="{{$item->id}}">{{$item->packet_name}}  
                            <span class="checkmarks" style="margin-right:5px;"></span>
                            <span class="tooltiptext" style="left:50%;">
                                <h5>Isi paket</h5>
                                <ul>
                                    @foreach($item->detailPacket as $paketitem)
                                    <li>
                                        {{$paketitem->food->food_name}} 
                                    </li>
                                    @endforeach    
                                </ul>
                            </span>   
                        </label>
                            @endforeach
                        </label>
                        
                        <label class="package-checkbox">
                                <h4>Paket Siang</h4>
    
                                @foreach ($siang as $item)
                            <label class="checkBoxCustom">
                                <input type="checkbox" name="default_packet[]" value="{{$item->id}}">{{$item->packet_name}}  
                                <span class="checkmarks" style="margin-right:5px;"></span>   
                                <span class="tooltiptext" style="left:50%;">
                                        <h5>Isi paket</h5>
                                        <ul>
                                            @foreach($item->detailPacket as $paketitem)
                                            <li>
                                                {{$paketitem->food->food_name}} 
                                            </li>
                                            @endforeach    
                                        </ul>
                                    </span>
                            </label>
                                @endforeach
                            </label>

                            <label class="package-checkbox">
                                    <h4>Paket Malam</h4>
        
                                    @foreach ($malam as $item)
                                <label class="checkBoxCustom">
                                    <input type="checkbox" name="default_packet[]" value="{{$item->id}}">{{$item->packet_name}}  
                                    <span class="checkmarks" style="margin-right:5px;"></span>   
                                    <span class="tooltiptext" style="left:50%;">
                                            <h5>Isi paket</h5>
                                            <ul>
                                                @foreach($item->detailPacket as $paketitem)
                                                <li>
                                                    {{$paketitem->food->food_name}} 
                                                </li>
                                                @endforeach    
                                            </ul>
                                        </span>
                                </label>
                                    @endforeach
                                </label>
                        <button type="submit" class="buttonSubmit">Submit</button>
                    </form>
                </div>
            </div>


            <!-- PAKET CUSTOM -->


            <div class="container2">
                <div class="headerContainer2">
                    <h2 style="text-align: center;font-family: Georgia, 'Times New Roman', Times, serif">List Menu</h2>
                </div>
                <div class="contentContainer2">
                    <h4 style="text-align: center">Paket Custom</h4>
                    <div class="paketCustom">

                        <div class="paketCustomLuarKiri">
                            <div class="paketanCustom">
                                <h6>Paket Pagi</h6>
                                <form action="/user/store-custom-packet/{{$user->id}}" method="POST">
                                    @csrf
                                    <div class="paketanContent">
                                        <div class="paketanContentKiri">
                                            <input type="hidden" name="duration" value="pagi" />
                                            @foreach($foods as $food)
                                            <label class="checkBoxCustom">
                                            <input 
                                                type="checkbox"
                                                name="custom_food[]"
                                                value="{{$food->id}}"
                                            />{{$food->food_name}}
                                            <span class="checkmarks"></span>
                                                <span class="tooltiptext" style="max-width:300px;">
                                                <img src="{{asset('storage/'.$food->image)}}" alt="{{$food->image}}" style="max-width:200px;max-height:100px">
                                                        Description : {{$food->description}} <br>
                                                        Price : {{$food->price}}
                                                </span>   
                                            </label>
                                            @endforeach
                                        </div>
                                        
                                    </div>
                                    <button type="submit" class="buttonSubmit">Submit</button>
                                </form>
                            </div>


                            <div class="paketanCustom">
                                <h6>Paket Siang</h6>
                                <form method="post" action="/user/store-custom-packet/{{$user->id}}">
                                    @csrf
                                    <div class="paketanContent">

                                        <div class="paketanContentKiri">
                                            <input type="hidden" name="duration" value="siang" />
                                            @foreach($foods as $food)
                                            <label class="checkBoxCustom">
                                            <input 
                                                type="checkbox"
                                                name="custom_food[]"
                                                value="{{$food->id}}"
                                            />{{$food->food_name}}
                                            <span class="checkmarks"></span>    
                                            <span class="tooltiptext" style="max-width:300px;">
                                                    <img src="{{asset('storage/'.$food->image)}}" alt="{{$food->image}}" style="max-width:200px;max-height:100px">
                                                            Description : {{$food->description}} <br>
                                                            Price : {{$food->price}}
                                                    </span>
                                            </label>
                                            @endforeach
                                        </div>
                                    </div>
                                    <button type="submit" class="buttonSubmit">Submit</button>
                                </form>
                            </div>
                        </div>

                        <div class="paketCustomLuarKanan">
                            <div class="paketanCustom">
                                <h6>Paket Malam</h6>
                                <form method="post" action="/user/store-custom-packet/{{$user->id}}">
                                    @csrf
                                    <div class="paketanContent">

                                        <div class="paketanContentKiri">
                                            <input type="hidden" name="duration" value="malam" />
                                            @foreach($foods as $food)
                                            <label class="checkBoxCustom">
                                            <input 
                                                type="checkbox"
                                                name="custom_food[]"
                                                value="{{$food->id}}"
                                            />{{$food->food_name}}
                                            <span class="checkmarks"></span>
                                            <span class="tooltiptext" style="max-width:300px;">
                                                    <img src="{{asset('storage/'.$food->image)}}" alt="{{$food->image}}" style="max-width:200px;max-height:100px">
                                                            Description : {{$food->description}} <br>
                                                            Price : {{$food->price}}
                                                    </span>
                                            </label>
                                            @endforeach
                                        </div>
                                    </div>
                                    <button type="submit" class="buttonSubmit">Submit</button>
                                </form>
                            </div>

                        </div>


                    </div>

                </div>




            </div>


        </div>


        <footer id="footer" class="footer">


            <div class="copyright">

                <p>Made with <i class="fa fa-heart" style="width: 20px; height:20px;color: red;"></i> by
                    Cateringku.com 2019. All Rights
                    Reserved</p>
            </div>

        </footer>

        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.0/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>


</body>

</html>