<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <meta http-equiv="X-UA-Compatible" content="ie=edge" />
        <title>Edit Packet</title>
    </head>
    <body>
        <form method="POST" action="/update-packet-name/{{$packet->id}}">
            @csrf
            <input name="packet_name" placeholder="{{$packet->packet_name}}" />
        <input name="price" placeholder="{{$packet->price}}">
            <button type="submit">Update</button>
        </form>
    </body>
</html>
