<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Packet;
use Illuminate\Support\Facades\Auth;
use App\Food;
use App\CustomPacket;

class MemberController extends Controller
{
    //

    public function index(){
        $pagi = Packet::where('duration','pagi')->get();
        $siang = Packet::where('duration','siang')->get();
        $malam = Packet:: where('duration','malam')->get();
        $user = Auth::user();
        $foods = Food::all();
        $customPagi = CustomPacket::where('duration','pagi')->get();
        $customSiang = CustomPacket::where('duration','siang')->get();
        $customMalam = CustomPacket::where('duration','malam')->get();
        return view('member_home',compact('pagi','siang','malam','user','foods','customPagi','customSiang','customMalam'));
    }
}
