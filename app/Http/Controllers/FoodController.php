<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Food;
use Illuminate\Support\Facades\Storage;

class FoodController extends Controller
{
    //

    public function create(){
        return view('create_food');
    }

    public function store(Request $request){
        $file = $request->file('image');
        $filename = 'food-'.$request['food_name'].'.'.$file->getClientOriginalExtension();
        $path=$file->storeAs('public',$filename);
        $food = new Food();
        $food->food_name= $request['food_name'];
        $food->image=$filename;
        $food->description = $request['description'];
        $food->price=$request['price'];
        $food->save();
        return redirect('/home');
    }
    public function edit($id){
        $food = Food::find($id);
        return view('edit_food',compact('food'));
    }
    public function update(Request $request, $id){
        $file = $request->file('image');
        $filename = 'food-'.$request['food_name'].'.'.$file->getClientOriginalExtension();
        $path=$file->storeAs('public',$filename);
        $food = Food::find($id);
        $oldpath = 'public/'.$food->image;
        Storage::delete($oldpath);
        $food->food_name= $request['food_name'];
        $food->image=$filename;
        $food->description = $request['description'];
        $food->save();
        return redirect('/home');
    }

    public function destroy($id){
        $food = Food::find($id);
        $oldpath='/public/'.$food->image;
        Storage::delete($oldpath);
        $food->delete();
        return redirect('/home');
    }
}
