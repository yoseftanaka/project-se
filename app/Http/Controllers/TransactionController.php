<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use App\HeaderTransaction;
use App\DetailTransaction;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Cart;
use App\Packet;

class TransactionController extends Controller
{
    public function storeTransaction(Request $request, $id){
        $headTransaction = new HeaderTransaction;
        $headTransaction->date = date('Y-m-d',time());
        $headTransaction->user_id = Auth::id();
        $headTransaction->nama = $request['nama'];
        $headTransaction->alamat = $request['alamat'];
        $headTransaction->telepon = $request['telepon'];
        $headTransaction->save();

        $detailTransaction = new DetailTransaction;
        $detailTransaction->header_id = $headTransaction->id;
        $detailTransaction->custom_packet_id = $id;
        $detailTransaction->quantity = $request['quantity'];
        $detailTransaction->save();

        $saved_id = $headTransaction->id;
        $header = HeaderTransaction::find($saved_id);
        $food_price = DetailTransaction::where('header_id',$saved_id)->with('customPacket')->get()->first();
        $total = $food_price->customPacket->price * $food_price->quantity;
        return view('payment',compact('saved_id','food_price','total'));
    }

    public function completeTransaction(Request $request,$id){
        $headTransaction = HeaderTransaction::find($id);
        $headTransaction->payment_type = $request['payment_type'];
        $headTransaction->save();
        return redirect('user/home');
    }

    public function forwardTransaction(Request $request, $id){
        $total = 0;
        $headTransaction = new HeaderTransaction;
        $headTransaction->date = date('Y-m-d',time());
        $headTransaction->user_id = Auth::id();
        $headTransaction->nama = $request['nama'];
        $headTransaction->alamat = $request['alamat'];
        $headTransaction->telepon = $request['telepon'];
        $headTransaction->save();

        $headtransaction_id = $headTransaction->id;
        $cart = Cart::all();
        for ($a=0;$a<sizeof($cart);$a++){
            $detailTransaction = new DetailTransaction;
            $detailTransaction->header_id = $headtransaction_id;
            $detailTransaction->packet_id = $cart[$a]->packet_id;
            $detailTransaction->quantity = $request['quantity'];
            $detailTransaction->save();
            $b = Packet::find($cart[$a]->packet_id);
            $total += $b->price;
        }
        $header = HeaderTransaction::find($headtransaction_id);
        $packet_price = DetailTransaction::where('header_id',$headtransaction_id)->with('packet')->get();
        Cart::whereNotNull('id')->delete();
        return view('payments',compact('total','packet_price','headtransaction_id'));
    }
}
