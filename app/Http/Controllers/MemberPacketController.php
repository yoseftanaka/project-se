<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Packet;
use App\DetailPacket;
use App\CustomPacket;
use Illuminate\Support\Facades\Auth;
use App\DetailCustomPacket;
use App\Food;
use App\Cart;

class MemberPacketController extends Controller
{
    public function viewPacket($id){
        $packet = Packet::find($id);
        $items = DetailPacket::with('food')->where('packet_id',$id)->get();
        return view('view_packet',compact('packet','items'));
    }

    public function viewCustomPacket($id){
        $packet = CustomPacket::find($id);
        $items = DetailCustomPacket::with('food')->where('custom_packet_id',$id)->get();
        return view('custom_view_packet',compact('packet','items'));
    }

    public function storePacket(Request $request, $id){
        $price =0;
        for ($a=0;$a<sizeof($request['custom_food']);$a++){
            $food = Food::find($request['custom_food'][$a]);
            $price+=$food->price;
            
        }
        $customPacket = new CustomPacket();
        $customPacket->user_id = $id;
        $customPacket->price = $price;
        $customPacket->duration = $request['duration'];
        $customPacket->save();

        for($a=0;$a<sizeof($request['custom_food']);$a++){
            $customDetailPacket = new DetailCustomPacket();
            $customDetailPacket->custom_packet_id= $customPacket->id;
            $customDetailPacket->food_id = $request['custom_food'][$a];
            $customDetailPacket->save();
        }
        $saved_id = $customPacket->id;
        return view('task_order',compact('saved_id'));
    }

    public function forwardPacket(Request $request, $id){
        for($a=0; $a<sizeof($request['default_packet']);$a++){
            $cart = new Cart();
            $cart->packet_id = $request['default_packet'][$a];
            $cart->save();
        }
        return view('task_orders',compact('id'));
    }
}
