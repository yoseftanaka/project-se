<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\DetailPacket;
use App\Packet;
use App\Food;

class PacketController extends Controller
{
    public function create(){
        return view('create_packet');
    }

    public function store(Request $request){
        $packet = new Packet();
        $packet->packet_name = $request['packet_name'];
        $packet->price = $request['price'];
        $packet->duration = $request['duration'];
        $packet->save();
        return redirect('/home');
    }

    public function editName($id){
        $packet = Packet::find($id);
        return view('edit_packet_name',compact('packet'));
    }
    public function updateName(Request $request, $id){
        $packet = Packet::find($id);
        $packet->packet_name = $request['packet_name'];
        $packet->price = $request['price'];
        $packet->save();
        return redirect('/home');
    }

    public function editItems($id){
        $packet = Packet::find($id);
        $items = DetailPacket::with('food')->where('packet_id',$id)->get();
        $foods = Food::all();
        return view('edit_packet_items',compact('packet','items','foods'));
    }

    public function updateItems(Request $request,$id){
        $detail_packet = new DetailPacket();
        $detail_packet->packet_id = $id;
        $detail_packet->food_id = $request['food_id'];
        $detail_packet->save();
        return redirect('/edit-packet-items/'.$id);
    }

    public function removeItem($id,$packet_id){
        $detail_packet = DetailPacket::find($id);
        $detail_packet->delete();
        return redirect('/edit-packet-items/'.$packet_id);
    }

    public function removePacket($id){
        $packet = Packet::find($id);
        $packet->delete();
        return redirect("/home");
    }
}
