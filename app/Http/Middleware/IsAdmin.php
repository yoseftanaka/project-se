<?php

namespace App\Http\Middleware;
use Illuminate\Support\Facades\Auth;

use Closure;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check() && $request->user()->role=="admin"){
            return $next($request);
        }
        else if(Auth::check() &&$request->user()->role=="member"){
            return redirect("/user/home");
        }
        else{
            return redirect("/");
        }
        
    }
}
