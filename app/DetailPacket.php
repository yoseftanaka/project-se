<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailPacket extends Model
{
    protected $fillable=['packet_id','food_id'];

    public function packet(){
        return $this->belongsTo('App\Packet');
    }
    public function food(){
        return $this->belongsTo('App\Food');
    }
}
