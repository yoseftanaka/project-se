<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Packet extends Model
{
    protected $filable=['packet_name','price','duration'];

    public function detailTransaction(){
        return $this->hasMany('App\DetailTransaction');
    }
    public function detailPacket(){
        return $this->hasMany('App\DetailPacket');
    }
}
