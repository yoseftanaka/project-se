<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CustomPacket extends Model
{
    protected $fillable=['custom_packet_name','price','user_id'];

    public function user(){
        return $this->belongsTo('App\User');
    }
    public function detailCustomPacket(){
        return $this->hasMany('App\DetailCustomPacket');
    }

    public function detailTransaction(){
        return $this->hasMany('App\DetailTransaction');
    }
}
