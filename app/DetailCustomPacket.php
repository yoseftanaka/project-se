<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailCustomPacket extends Model
{
    protected $fillable=['custom_packet_id','food_id'];

    public function customPacket(){
        return $this->belongsTo('App\CustomPacket');
    }

    public function food(){
        return $this->belongsTo('App\Food');
    }
}
