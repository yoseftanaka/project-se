<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HeaderTransaction extends Model
{
    protected $fillable=['date','user_id'];

    public function user(){
        return $this->belongsTo('App\User');
    }

    public function detailTransaction(){
        return $this->hasMany('App\DetailTransaction');
    }
}
