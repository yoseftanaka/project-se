<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Food extends Model
{
    protected $fillable = ['food_name','image','description','price'];

    public function detailPacket(){
        return $this->hasMany('App\DetailPacket');
    }
    public function detailCustomPacket(){
        return $this->hasMany('App\DetailCustomPacket');
    }
}
