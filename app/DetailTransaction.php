<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DetailTransaction extends Model
{
    protected $fillable=['header_id','packet_id'];

    public function headerTransaction(){
        return $this->belongsTo('App\HeaderTransaction');
    }
    public function packet(){
        return $this->belongsTo('App\Packet');
    }

    public function customPacket(){
        return $this->belongsTo('App\CustomPacket');
    }
}
