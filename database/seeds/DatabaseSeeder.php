<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        DB::table('users')->insert([
            'name' => 'Yosef Tanaka',
            'email' => 'tanakayosef40@gmail.com',
            'password' => bcrypt('secret'),
            'role' => 'admin',
        ]); 
        DB::table('users')->insert([
            'name' => 'Member',
            'email' => 'member@gmail.com',
            'password' => bcrypt('secret'),
        ]); 
    }
}
