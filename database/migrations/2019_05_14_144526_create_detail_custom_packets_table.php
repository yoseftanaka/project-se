<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailCustomPacketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_custom_packets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('custom_packet_id')->unsigned();
            $table->bigInteger('food_id')->unsigned();
            $table->foreign('custom_packet_id')->references('id')->on('custom_packets')->onDelete('cascade');
            $table->foreign('food_id')->references('id')->on('foods')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_custom_packets');
    }
}
