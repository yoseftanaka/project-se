<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailPacketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_packets', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('packet_id')->unsigned();
            $table->bigInteger('food_id')->unsigned()->nullable();
            $table->foreign('packet_id')->references('id')->on('packets')->onDelete('cascade');
            $table->foreign('food_id')->references('id')->on('foods')->onDelete('cascade');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_packets');
    }
}
