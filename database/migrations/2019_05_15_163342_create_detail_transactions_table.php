<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('header_id')->unsigned();
            $table->bigInteger('packet_id')->unsigned()->nullable();
            $table->foreign('header_id')->references('id')->on('header_transactions');
            $table->foreign('packet_id')->references('id')->on('packets');
            $table->bigInteger('custom_packet_id')->unsigned()->nullable();
            $table->foreign('custom_packet_id')->references('id')->on('custom_packets');
            $table->biginteger('quantity');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_transactions');
    }
}
