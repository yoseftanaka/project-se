<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware'=>['isadmin']],function(){
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('/create-food','FoodController@create');
    Route::post('/store-food','FoodController@store');
    Route::get('/edit-food/{id}','FoodController@edit');
    Route::post('/update-food/{id}','FoodController@update');
    Route::get('/delete-food/{id}','FoodController@destroy');
    Route::get('/create-packet','PacketController@create');
    Route::post('/store-packet','PacketController@store');
    Route::get('/edit-packet-name/{id}','PacketController@editName');
    Route::get('/edit-packet-items/{id}','PacketController@editItems');
    Route::post('/update-packet-name/{id}','PacketController@updateName');
    Route::post('/update-packet-items/{id}','PacketController@updateItems');
    Route::get('/remove-packet-item/{id}/{packet_id}','PacketController@removeItem');
    Route::get('/delete-packet/{id}','PacketController@removePacket');
});
Route::group(['middleware'=>['ismember']],function(){
    Route::get('/user/home','MemberController@index');
    Route::get('/user/view-packet/{id}','MemberPacketController@viewPacket');
    Route::get('/user/custom-view-packet/{id}','MemberPacketController@viewCustomPacket');
    Route::post('/user/store-custom-packet/{user_id}','MemberPacketController@storePacket');
    Route::post('user/store-transaction/{packet_id}','TransactionController@storeTransaction');
    Route::post('/user/complete-transaction/{header_id}','TransactionController@completeTransaction');
    Route::post('/user/forward-packet/{user_id}','MemberPacketController@forwardPacket');
    Route::post('/user/forward-transaction/{id}','Transactioncontroller@forwardTransaction');
});



